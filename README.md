# My fifth rice
![image](https://i.imgur.com/bB1cakL.png)

Programs: 
- qutebrowser
- lolcat + figlet
- neovim
- slurm (network usage viewer)

## Dependencies:
- nvim
- Tryone's compton fork
- st
- bspwm, sxhkd and xdo
- dunst
